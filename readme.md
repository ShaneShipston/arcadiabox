# Arcadia Box

Current Version: `1.1.x`

## Requirements

* [Vagrant](http://vagrantup.com/)
* [VirtualBox](https://www.virtualbox.org/)

## Getting started

1. Make any edits you'd like within config.json
2. To get started run `vagrant up`
3. You can access your WordPress install from [arcadia.local](http://arcadia.local/)

## Set up Host

If this is your first time using Arcadia Box you will need to:

1. Add `192.168.33.10 arcadia.local` to your hosts file
