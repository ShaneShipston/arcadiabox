<?php

/**
 * Broken Link Checker
 */
$blc = json_decode(get_option('wsblc_options'));
$blc->send_email_notifications = false;

update_option('wsblc_options', json_encode($blc));

/**
 * Disable Comments
 */
$dc = get_option('disable_comments_options');
$dc['remove_everywhere'] = true;
$dc['disabled_post_types'] = [
    'post',
    'page',
    'attachment'
];

update_option('disable_comments_options', $dc);
