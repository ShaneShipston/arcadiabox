#!/usr/bin/env bash

RED='\033[0;31m' # error
GRN='\033[0;32m' # success
BLU='\033[0;34m' # task
BRN='\033[0;33m' # headline
NC='\033[0m' # no color

config=$(jq '.' config.json)

folder=$(jq -r '.folder' <<< $config)
theme=$(jq -r '.theme' <<< $config)
frontpage=$(jq -r '.frontpage' <<< $config)
admin=$(jq -r '.admin' <<< $config)
db=$(jq -r '.db' <<< $config)
settings=$(jq -r '.settings' <<< $config)

timezone=$(jq -r '.timezone' <<< $settings)
permalink=$(jq -r '.permalink' <<< $settings)
description=$(jq -r '.description' <<< $settings)

readarray -t active_plugins < <(jq -r '.plugins.active[]' <<< ${config})
readarray -t inactive_plugins < <(jq -r '.plugins.inactive[]' <<< ${config})
readarray -t pages < <(jq -r '.pages[]' <<< ${config})

# Create public folder
if [ ! -d $folder ]; then
  mkdir $folder
fi

cd $folder

# Install WordPress
printf "${BRN}=== INSTALL WORDPRESS ===${NC}\n"
wp core download \
  --locale=$(jq -r '.locale' <<< $settings)

wp core config \
  --dbname=$(jq -r '.name' <<< $db) \
  --dbuser=$(jq -r '.user' <<< $db) \
  --dbpass=$(jq -r '.pass' <<< $db) \
  --dbprefix=$(jq -r '.prefix' <<< $db) \
  --locale=$(jq -r '.locale' <<< $settings)

wp core install \
  --url=$(jq -r '.url' <<< $settings) \
  --title="$(jq -r '.title' <<< $settings)" \
  --admin_user=$(jq -r '.user' <<< $admin) \
  --admin_password=$(jq -r '.password' <<< $admin) \
  --admin_email=$(jq -r '.email' <<< $admin) \
  --skip-email

# Settings
printf "${BRN}=== SETTINGS ===${NC}\n"
wp option update timezone $timezone
wp option update timezone_string $timezone

wp rewrite structure $permalink
wp rewrite flush --hard

wp option update blogdescription "$description"
wp option update blog_public 0

# create and set frontpage
if [ ${frontpage+x} ]; then
  wp post create \
    --post_type=page \
    --post_title="$frontpage" \
    --post_content='' \
    --post_status=publish

  frontpageid=$(wp post list \
    --post_type=page \
    --post_status=publish \
    --posts_per_page=1 \
    --pagename="$frontpage" \
    --field=ID \
    --format=ids)

  wp option update page_on_front $frontpageid
  wp option update show_on_front 'page'
fi

# Clone latest version of Arcadia
sudo -u vagrant \
  -H git clone \
  --depth 1 \
  -b v3.2.x \
  https://ShaneShipston@bitbucket.org/ShaneShipston/arcadia.git wp-content/themes/$theme

BLANK_STYLESHEET='/*
Theme Name: Arcadia
Theme URI: http://shout-media.ca
Description: Custom WordPress theme
Author: Shout! Media
Author URI: http://shout-media.ca
Version: 1.0
*/'

echo "$BLANK_STYLESHEET" | tee wp-content/themes/$theme/style.css

# Activate Theme
wp theme activate $theme

# Menu Setup
wp menu create "Main Menu"
wp menu location assign main-menu primary
wp menu location assign main-menu secondary
wp menu item add-post main-menu 3

for entry in "${pages[@]}"
do
  wp post create \
    --post_type=page \
    --post_title="$entry" \
    --post_content='' \
    --post_status=publish

  newpageid=$(wp post list \
    --post_type=page \
    --post_status=publish \
    --posts_per_page=1 \
    --pagename="$entry" \
    --field=ID \
    --format=ids)

  wp menu item add-post main-menu $newpageid
done

# Clean up
printf "${BRN}=== CLEANUP ===${NC}\n"
wp comment delete 1 --force
wp post delete 1 2 --force

if [ -f readme.html ]; then
  rm readme.html;
fi

if [ -f license.txt ]; then
  rm license.txt;
fi

wp theme delete twentyfifteen twentysixteen twentyseventeen
wp plugin delete akismet hello

# Plugins
printf "${BRN}=== PLUGINS ===${NC}\n"

for entry in "${active_plugins[@]}"
do
	wp plugin install $entry --activate
done

for entry in "${inactive_plugins[@]}"
do
  wp plugin install $entry
done

# get plugin path
acf_zip_file="$(wp plugin path)/acf-pro.zip"

# get acf zip file
wget -O ${acf_zip_file} "http://connect.advancedcustomfields.com/index.php?p=pro&a=download&k=b3JkZXJfaWQ9MzQ4NzV8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTA3LTE1IDE5OjU1OjU1" --quiet

# install & activate acf
wp plugin install ${acf_zip_file} --activate

# remove zip file
rm ${acf_zip_file}

# update internal plugin settings
wp eval-file ../update-config.php

printf "${BRN}========== FINISHED ==========${NC}\n"
