#!/usr/bin/env bash

# Install jq
apt-get install -y jq

# Change into root directory
cd /var/www

# Start with a clean folder
rm -rf public

# Fix Line Endings
dos2unix install.sh
dos2unix update.sh

# Install WordPress
sudo -u vagrant bash install.sh
