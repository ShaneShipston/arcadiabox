#!/usr/bin/env bash

# Change Directory
cd /var/www/public

# Update WordPress
wp core update

# Update Plugins
wp plugin update --all
